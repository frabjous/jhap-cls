\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jhap}[2023/05/03 Journal for the History of Analytical Philosophy]
\LoadClass[11pt,twocolumn]{article}

\newif\ifreview
\reviewfalse
\newif\ifeditedvolume
\editedvolumefalse
\newif\ifauthorsaseditors
\authorsaseditorsfalse
\newif\ifinfootnote
\infootnotefalse

\DeclareOption{review}{\reviewtrue}
\DeclareOption{edited}{\authorsaseditorstrue}
\ProcessOptions\relax

\RequirePackage[margin=0.97in]{geometry}
\geometry{letterpaper}
\geometry{landscape}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{graphicx}
\RequirePackage{amssymb}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\titleformat{\section}[hang]{\sffamily\bfseries\large\raggedright}{\thesection.}{0.5ex}{}
\titleformat{\subsection}[hang]{\sffamily\bfseries\raggedright}{\thesubsection.}{0.5ex}{}
\titleformat{\subsubsection}[hang]{\small\sffamily\bfseries\raggedright}{\thesubsubsection.}{0.5ex}{}
\RequirePackage[osf]{newpxtext}
\RequirePackage{newpxmath}
\RequirePackage{natbib}
\RequirePackage{enumitem}

\setlength{\parskip}{0pt}
\setlength{\partopsep}{0pt}
\setlength{\columnsep}{40pt}

\newcommand{\columnbreak}{\newpage}
\renewcommand{\headrulewidth}{0pt}

\fancyhead{}

\fancyfoot{}

\fancyfoot[R]{\small{\textit{\textsf{\fontseries{l}\selectfont Journal for the History of Analytical Philosophy vol.\ \thevolume\ no.\ \thevolnumber}}} \ \ \ \ [\(\thepage\)]}

%Open Sans for selections
\RequirePackage[defaultsans]{opensans}

%define font size for journal title
\newcommand*\journaltitlesize{%
  \@setfontsize\journaltitlesize{23.7}{40}}


%renew footnotecommand to check if inside
\let\origfootnote\footnote
\renewcommand{\footnote}[1]{\infootnotetrue\origfootnote{#1}\infootnotefalse}

%reduce font size in quoted text
\renewenvironment{quote}
               {\list{}{\rightmargin\leftmargin}%
                \setlength{\parskip}{0pt}%
                \item\relax\ifinfootnote\else\small\fi}
               {\endlist}
               
\RequirePackage{etoolbox}
\patchcmd{\quote}{\rightmargin}{\leftmargin .7em \rightmargin}{}{}

% code for global renewcommands
\def\gnewcommand{\g@star@or@long\gnew@command}
\def\grenewcommand{\g@star@or@long\grenew@command}
\def\g@star@or@long#1{% 
  \@ifstar{\let\l@ngrel@x\global#1}{\def\l@ngrel@x{\long\global}#1}}
\def\gnew@command#1{\@testopt{\@gnewcommand#1}0}
\def\@gnewcommand#1[#2]{%
  \kernel@ifnextchar [{\@gxargdef#1[#2]}%
                {\@argdef#1[#2]}}
\let\@gxargdef\@xargdef
\patchcmd{\@gxargdef}{\def}{\gdef}{}{}
\let\grenew@command\renew@command
\patchcmd{\grenew@command}{\new@command}{\gnew@command}{}{}


\RequirePackage[colorlinks=true,allcolors={[rgb]{0,0,0.7}}]{hyperref}

\RequirePackage{tocloft}
\setlength{\cftbeforesecskip}{0pt}

\setcitestyle{authoryear,aysep={}}

% non-french spacing but without extra stretch; see https://tex.stackexchange.com/questions/412189/is-it-possible-to-have-non-french-spacing-without-extra-stretch
\xspaceskip=\fontdimen2\font plus \fontdimen3\font minus \fontdimen4\font
\advance\xspaceskip by \fontdimen7\font

%% if we want endnotes
%\RequirePackage{endnotes}
%\let\footnote=\endnote
\newcommand{\copynotice}{}
\newcommand{\thevolume}{?}
\newcommand{\volume}[1]{\grenewcommand{\thevolume}{#1}}
\newcommand{\thevolnumber}{?}
\newcommand{\volnumber}[1]{\grenewcommand{\thevolnumber}{#1}}
\newcommand{\theauthor}{The Author}
\renewcommand{\author}[1]{\grenewcommand{\theauthor}{#1}}
\newcommand{\thetitle}{The Title}
\renewcommand{\title}[1]{\grenewcommand{\thetitle}{#1}}
\newcommand{\thesubtitle}{}
\newcommand{\subtitle}[1]{\grenewcommand{\thesubtitle}{#1}}
\newcommand{\theaffiliation}{Affiliation}
\newcommand{\affiliation}[1]{\grenewcommand{\theaffiliation}{#1}}
\newcommand{\theemail}{someone@somewhere.com}
\newcommand{\email}[1]{\grenewcommand{\theemail}{#1}}

%for multiple authors
\newcommand{\thesecondauthor}{}
\newcommand{\secondauthor}[1]{\grenewcommand{\thesecondauthor}{#1}}
\newcommand{\thesecondaffiliation}{}
\newcommand{\secondaffiliation}[1]{\grenewcommand{\thesecondaffiliation}{#1}}
\newcommand{\thesecondemail}{}
\newcommand{\secondemail}[1]{\grenewcommand{\thesecondemail}{#1}}
\newcommand{\thespecvol}{}
\newcommand{\specialvolume}[1]{\grenewcommand{\thespecvol}{#1}}
\newcommand{\thespecvoleditors}{}
\newcommand{\specialvolumeeditors}[1]{\grenewcommand{\thespecvoleditors}{#1}}

%nudge command
\newcommand{\nudge}{\hspace*{0.07em}}

%repeated author bibline
\newcommand{\rbibrule}{\rule[0.5ex]{2.5em}{0.5pt}}

%for reviews
\ifreview
    \newcommand{\thereviewedtitle}{Reviewed Title}
    \newcommand{\reviewedtitle}[1]{\grenewcommand{\thereviewedtitle}{#1}}
    \newcommand{\thereviewedauthor}{Reviewed Author}
    \newcommand{\reviewedauthor}[1]{\grenewcommand{\thereviewedauthor}{#1}} 
    \newcommand{\thereviewededitor}{Reviewed Editor}
    \newcommand{\reviewededitor}[1]{\global\editedvolumetrue\grenewcommand{\thereviewededitor}{#1}} 
    \newcommand{\thesecondreviewedauthor}{}
    \newcommand{\secondreviewedauthor}[1]{\grenewcommand{\thesecondreviewedauthor}{#1}}
    \newcommand{\thesecondreviewededitor}{}
    \newcommand{\secondreviewededitor}[1]{\grenewcommand{\thesecondreviewededitor}{#1}}
    \newcommand{\thereviewedpubdetails}{Place: Publisher, Year. Pages. \$ Cost Hardcover. ISBN XXX-X-XX-XXXXXXX}
    \newcommand{\reviewedpubdetails}[1]{\grenewcommand{\thereviewedpubdetails}{#1}} 
\fi

\newcommand{\signoff}{

\bigskip
{ \flushright
\sffamily\large \textbf{\theauthor}\\
\theaffiliation\\
\theemail\par

\ifdefempty{\thesecondauthor}{}{%
\bigskip
\textbf{\thesecondauthor}\\
\thesecondaffiliation\\
\thesecondemail\par
}

}
}

\AtBeginDocument{% BEGIN DOCUMENT
\newcommand{\theallauthors}{\theauthor\ifdefempty{\thesecondauthor}{}{\ and \thesecondauthor}}
\ifreview 
    \ifeditedvolume
        \newcommand{\theallreviewededitors}{\thereviewededitor\ifdefempty{\thesecondreviewededitor}{}{\ and \thesecondreviewededitor}}
    \else
        \newcommand{\theallreviewedauthors}{\thereviewedauthor\ifdefempty{\thesecondreviewedauthor}{}{\ and \thesecondreviewedauthor}}
    \fi
    \renewcommand{\thetitle}{Review of \thereviewedtitle}
\fi

\hypersetup{%
    pdfauthor={\theallauthors},
    pdftitle={\thetitle},
    pdfsubject={History of Philosophy}
}




\pagestyle{fancy}


\setcounter{page}{0}


\thispagestyle{empty} %for first page


{\sffamily

\begin{flushright}{\journaltitlesize{\fontseries{eb}\selectfont Journal for the History of\\[0.1\baselineskip] Analytical Philosophy}}\\[0.3\baselineskip] 

\huge{\fontseries{l}\selectfont Volume \thevolume, Number \thevolnumber}\end{flushright}
\begin{flushright}
\textbf{Editor in Chief}\\
Audrey Yap, University of Victoria

\vspace{0.7\baselineskip}
\textbf{Editorial Board}\\
Annalisa Coliva, UC Irvine\\
Vera Flocke, Indiana University, Bloomington\\
Henry Jackman, York University\\
Kevin C.\ Klement, University of Massachusetts\\
Consuelo Preti, The College of New Jersey\\
Marcus Rossberg, University of Connecticut\\
Sanford Shieh, Wesleyan University\\
Anthony Skelton, Western University\\
Mark Textor, King's College London

\vspace{0.7\baselineskip}
\textbf{Editor for Special Issues}\\
Frederique Janssen-Lauret, University of Manchester\\

\vspace{0.7\baselineskip}
\textbf{Review Editors}\\
Sean Morris, Metropolitan State University of Denver\\
Rachel Boddy, Utrecht University

\vspace{0.7\baselineskip}
\textbf{Design and Layout}\\
Daniel Harris, Hunter College\\
Kevin C.\ Klement, University of Massachusetts

\vspace{0.7\baselineskip}
ISSN: 2159-0303

\vspace{1\baselineskip}
{\Large\textit{%
\href{https://jhaponline.org}{jhaponline.org}%
}}

\vfill

\ifdefempty{\copynotice}{%
\copyright\ \the\year\ \theallauthors%
}{%
\copynotice%
}
\end{flushright}%
}

\columnbreak
\ifreview% review title page in lieu of abstract
    {
    \setlength{\parskip}{\baselineskip}
    \setlength{\parindent}{0in}
    \vspace*{2\baselineskip}
    {\sffamily\bfseries\large\noindent%
    \ifeditedvolume
        \theallreviewededitors, ed\ifdefempty{\thesecondreviewededitor}{}{s}.%
    \else
        \theallreviewedauthors{}.
    \fi%
    \ \textit{\thereviewedtitle}.\ \thereviewedpubdetails}\\[2pt]
    {\sffamily\large\noindent{}Reviewed by \theallauthors}\raggedright\par

    \vfill~

    \columnbreak
    }
\fi
} %end of BEGIN DOCUMENT commands

\AtEndDocument{% END DOCUMENT
%\end{multicols*}
} %END of END DOCUMENT commands 


%abstract on side of title page
\renewenvironment{abstract}{
    \setlength{\parskip}{\baselineskip}
    \setlength{\parindent}{0in}
    {\raggedright\vspace*{2\baselineskip}
    {\sffamily\bfseries\large\noindent \thetitle}\\[2pt]
    {\sffamily\large\noindent\ifauthorsaseditors{Edited by }\fi\theallauthors}}

    \bigskip
    \noindent\ignorespaces
}{ %
    \vfill\ifdefempty{\thespecvol}{~}{%
       \textsf{\textit{Special Issue:} \thespecvol\\
Edited by \thespecvoleditors }\\[-0.8ex] \phantom{x}
    }

    \columnbreak
}
\renewcommand{\maketitle}{%
\clearpage
\begin{center}

    \setlength{\baselineskip}{1.2\baselineskip}%
	{\sffamily
	\ifreview \Large{\textbf{Review: \textit{\thereviewedtitle}, \ifeditedvolume edited by \theallreviewededitors \else by \theallreviewedauthors \fi}} \else{\Large{\textbf{\thetitle}}}\ifdefempty{\thesubtitle}{}{\\[0.5\baselineskip] \textbf{\thesubtitle}}\fi\\
		\vspace{10pt}
	\Large{\ifauthorsaseditors{Edited by }\fi\theallauthors}\\
		\vspace{20pt}
	}
\end{center}
\noindent
  \begingroup
  \catcode13=10
  \@ifnextchar\relax
    {\endgroup}%
    {\endgroup}%
}
