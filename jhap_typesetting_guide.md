
# JHAP Typesetting Guide

## Instructions on typesetting an accepted article or review -- Overview

1. Log in to typesetting framework, currently located at: <http://kck-work.ddns.umass.edu/jhap/>. You should see a list of active projects.

2. Scroll to "Create new project" box near the bottom, and enter the four-digit document number that appears in the URL for the submission when it is viewed in OJS. (Currently, this is not implemented, but eventually the typesetting framework may be able to automate certain interactions with OJS, and will then require this number to be correct.) Click create.

3. You will see a form in which you enter various metadata (author's name, email, affiliation, the volume number, etc.). There is a radio box for using the settings for review, which require additional metadata. Fill in this information and click save.

4. There should now be a box for the piece in the list of projects on the main menu for the typesetting framework. There should now be a link for uploading a file. This accepts a variety of file formats, including most common Word Processor formats and markup languages, but an MS Word (.docx) file should work fine. It can even be a LaTeX file.

5. Once the main file is uploaded, supplementary files such as images and custom LaTeX packages an be uploaded if necessary.

6. There should also be a link to edit the bibliography. The bibliography should be edited and finalized before work on the main article is begun (though you can make changes to the bibliography later if needed). See the section on [editing the bibliography](#editbib) below. Completing most of the work on the bibliography before anything else helps greatly with automation of inserting citation hyperlinks.

7. Once the bibliography has been finalized, there is a link to "create LaTeX file"; clicking this should convert the uploaded file to LaTeX, and bring up the page to edit the result. See the section on [editing the main LaTeX file]{#editlatex} below.

8. When the version is ready to be viewed by the authors, click "Create proof set". This will bring up a version of the proof correction interface in which the editor can add query notes. Navigate through pages using the links at the top. Query notes are automatically saved when entered, and you can simply click the back button or close the window when done.

9. If you need to add additional notes to the proof set, you can use the "editor link" from the list on the main framework page. The "author link" is the URL you should email to the author(s), so that they can answet the queries, and add additional notes concerning other changes they would like made to the proofs. When they are done, they may send an email to the editor notifying that they are done.

10. The LaTeX file can be further edited in response to the author comments. Another proof set can be created afterwards to share the new set with the authors, and the previous few steps can be repeated.

11. When all changes have been made to the document, and the proofs are approved by the author(s), it is time to create a "Web optimized" or "linearized" PDF. There should be a link to create such a PDF in the box in the framework's main menu. Once it is created, there will be a link to download it. This is the file that should be uploaded as "galley" to the "Production" tab in OJS.

12. The "extract references" and "extract abstract" provide text (and in the later case, HTML markup) that can be used to fill in those metadata fields in OJS, replacing whatever the author filled in when the piece was first submitted. This way, what is shown in the public webpage for the volume or article will match what is in the piece when published.

13. Once a piece has been published, click the "archive" link to hide the piece from the menu subsequently. (You can bring up archives pieces by checking the "Show archives projects" box near the top.)

## Editing the bibliography {#editbib}

1. Click the edit bibliography link for the document in question.

2. If the uploaded file was a traditional Word Processor file, the framework attempts to determine which portion of the document is the bibliography, and to break it up into various entries. Each entry is listed separately with its own fields to be filled in. Please, note, however, that it often makes mistakes, as there is no guarantee the file provided will be formatted in the expected way. Therefore, you may need to make deletions and additions, etc.

3. If you were provided with a BibTeX file instead, its contents can be imported using the button at the top of the page. Note, however, that will import every entry in the BibTeX database, not just those cited in the document. (Maybe eventually this will be changed.)

4. Typically, each entry can be handled simply by selecting the right type of entry (using "incollection" for chapters in edited volumes) copying/pasting the relevant portion of the entry into the fields below. Sometimes additional information must be supplied, either by asking the author(s), or hunting it down yourself. (See "bibliography conventions" below.)

5. Once the fields are filled in, the "LaTeX bibliography item" should automatically fill in according to our bibliography format. This is the exact code that will be inserted into the `thebibliography` LaTeX environment. If the code appears correct, nothing further needs to be done for the entry.

6. Sometimes manual tweaks are necessary in the case of an unusual entry, or if additional information such as a translator, or citation abbreviation, is used. Clicking the "override" button allows you to tweak the entry. Notice, that once this is clicked, changes to the fields above no longer affect the entry, so do this after the fields have been filled in.

7. New entries can be added in between others using the "insert entry" bars. If changes require reshuffling the order of the entries, or if the author's own sorting was off, there is a "re-sort" button to rearrange the entries. 

8. Periodically click the "save" link at the bottom of the screen to save progress, but wait until all changes have been made to click "finalize". Once the bibliography has been finalized, any further changes to the bibliography should be done by manually editing the `thebibliography` environment inside the LaTeX file.

## Bibliography conventions

Some of these are admittedly a bit strange, as the bibliography style is more a result of evolution than design, with my imposing some new order on top of what I took to be the pre-existing conventions the journal had been using beforehand. Rethinking some of these conventions might not be a bad idea, but they are what the framework is currently set up to use.

* Here are some samples the style for articles, anthology chapters, books and websites, respectively:\
\
Russell, Bertrand, 1905. "On Denoting." _Mind_ 14: 479--93.\
Weiner, Joan, 1986. "Putting Frege in Perspective." In _Frege Synthesized_, edited by L. Haaparanta and J. Hintikka, pp. 9--27. Dordrecht: D. Reidel.\
Whitehead, A. N. and Bertrand Russell, 1925. _Principia Mathematica_, 3 vols., 2nd ed. Cambridge: Cambridge University Press.\
Zalta, Edward N., 2016. "Gottlob Frege." In _The Stanford Encyclopedia of Philosophy_,  <https://plato.stanford.edu/entries/frege/>, accessed 2 October 2018.

* We use full names in most cases for author names (or editor names if used in place of author names), rather than simply initials for given names, except when an author is best known by their initials (e.g., "G. E. Moore", etc.) Sometimes this requires looking up those names. Second and additional names are put in natural order, rather than with the surname first. However, when an editor, or translator, etc., is listed later in the entry, it is fine to abbreviate the given names. When two initials are used, they should be separated with a thin space (LaTeX: `\,`) e.g., `G.\,E. Moore`.

* Repeated names are replaced with a horizontal line ------; the LaTeX code (defined in the document class) is `\rbibrule`.

* *Page numbers*: "pp." is used for page numbers for chapters in larger volumes, but *not* for journal articles. I'm not exactly sure how this came about, but we've stuck with it for some time now.

* *Page ranges*: When a page range is given (whether in the bibliography or in a citation), the preference is to include exactly two digits after the dash, even when one would do (e.g., 122--24 not 122--4 or 122--124). Exceptions are for page numbers below 10, or when three or more digits are necessary to make the range clear (5--6, or 1202--311).

* *Citation of footnotes* in other works is given in the form 127 n 5 with narrow spaces (LaTeX: `\,`) before and after the "n". The second number may be omitted if it seems unnecessary. The abbreviation "fn." is never used. Crossreferences to notes in the article in question use the word "note"; e.g., "See note 15.", using the `\label` and `\ref` commands for hyperlinking.

* Journal volume numbers are listed but not issue numbers; years are given but not day or months, unless doing is necessary for some unusual reason (e.g., a periodical only uses issue numbers not volume numbers, or can only be only identified by date). Volume numbers always use arabic numerals, not Roman numerals.

* If more than one chapter in an edited volume is in the bibliography, the volume should get its own bibliographical entry, and the chapter entries should consist only of name, date, title and a `\citet{key}` citation (with page numbers) to it. If only one chapter is cited, then the full information for the volume should be included in its entry. A shorter version of the above would be:\
\
Weiner, Joan, 1986. "Putting Frege in Perspective." In Haaparanta and Hintikka (1986), pp. 9--27.\

* If an entry is alphabetized by editor(s), then "ed." or "eds." is used at the start of the entry before the date, but if an editor or translator is mentioned later, the full phrase "edited by", or "translated by" is used. Notice this goes *after* the title of the volume or translated piece, not before, in such cases.

* English names for cities are used rather than the native names, if different, e.g., "Munich", not "München"


## Editing the main LaTeX file {#editlatex}

The conversion process (which is done by pandoc) is far from perfect, so it is important to carefully clean up the result. Here are some tasks that typically need to be done, and some tips.

* It is vital during this process to have a PDF of the submited file, as the author prepared it, available for comparison.

* Although the conversion process does make an attempt to automate hyperlinking the citations, how well it does with this depends a lot on the consistency of the source document and accuracy of the bibliography. Usual, a lot of manual tweaking is necessary for the citations; see the citation convention section below.

* The conversion often fails to identify blockquotes, and these often need to be reinstated with `\begin{quote}` and `\end{quote}`. Note that if you skip a line after `\end{quote}` you'll get a new paragraph; if not, not. With other environments, you may need to use the `\indent` or `\noindent` commands to get the desired results. 

* Acknowledgements are placed in an unnumbered subsection after the body of the article but before the signoff; many authors attempt to use a footnote instead, and the contents should be moved into the subsection instead.

* Manual intervention for widow and orphan control (which should only be done quite late in the process) can be done with `\enlargethispage{0.5\baselineskip}` and similar commands. Negative values can be used. I usually try to find the optimal value (minimum change in either direction) testing increments of 0.1 times the baselineskip value.

## General typesetting conventions

* Section Headers Are In Title Case; Subsection headers have only an initial capital.

* Footnote markers go after end-of-sentence punctuation, not before, as here.¹

* Footnotes should never be *purely* citational, though it is fine if the main point of the footnote is introduce a citation and simultaneously make a comment on it.

* Internal crossreferences to, e.g., "Section 3", should have the word "Section" in uppercase. Use LaTeX's `\label` and `\ref` commands so that the number stays correct through changes, and is hyperlinked.

* Dates should be in the international format: (5 May 1915), not the US format: (May 5, 1915), if possible.

* "i.e.", "e.g." and "viz." should be followed by commas whenever possible, e.g., like this; inside direct quotations where there is no comma, be sure to use `\ ` after the period to avoid inter-sentence space.

* We use em-dashes (—; LaTeX: `---`) to break up text—like this—without spaces around them. We do not use en-dashes with spaces – like this – as some others do, and the latter punctuation style should be changed to the former even inside quotations. We use en-dashes (–; LaTeX: `--`) for number ranges only (e.g., 125–31). Do not confuse either of these symbols with the regular hyphen (LaTeX: `-`), used in hyphenated words (e.g., "space-time"), or the mathematical minus/subtraction sign − (LaTeX: `$-$`).

* However, many authors overuse the em-dash, so consider intervening and switching to commas, parentheticals, semi-colons, etc., as appropriate.

* Ellipses … are used for deletions from quotations, and hard brackets are used for insertions. There is therefore never any reason to use the hybrid monstrosity "[…]" which some authors seem to like. We prefer the spacing around ellipses to be compact, using the LaTeX code `\,\ldots{}` immediately between the words. The `\,` inserts a narrow space before the dots, and there is naturally a narrow space afterwords. No additional space is necessary.

* We do not insist on either British or American spellings; authors may use whichever they prefer.

* We do not insist on either single quotes or double quotes being used by default, but we do impose that one or the other conventions is used consistently, and authors' uses are often very inconsistent. If single quotes are used in the body, the definition given for `\enquote` in the `thebibliography` environment should be modified accordingly.

* We historically have not been fussy about whether commas go inside or outside quotation marks, and let the authors decide.

* Other than the above, we try to be hands off about making changes to the authors' prose. We do not enforce pointless rules such as not ending sentences with prepositions or not splitting infinitives.

## Citation conventions

* We use the LaTeX natbib package for its citation commands, usually either `\citet[page]{key}`, which gives (Name year, page), or `\citep[page]{key}`, for Name (year, page). The former is used for parenthetical citations in the main text, and the latter is used when the name (or a noun phrase generally) is needed grammatically for the sentence, which occurs more often in footnotes. "p." or "pp." are not used in citations.

* Using the natbib commands should take care of the hyperlinks; sometimes LaTeX needs to be run multiple times for these to work properly, and you may temporarily see question marks in the pdf output until the next run. If the question marks remain after multiple runs, likely the citation key is wrong or missing from the bibliography.

* Occasionally it is necessary to use other natbib citation commas such as `\citealt{key}` for Name year without parentheses, or commands such as `\citeyear` or `\citeauthor`. See the [natbib documentation](http://mirror.utexas.edu/ctan/macros/latex/contrib/natbib/natbib.pdf) for details.

* Parenthetical citations are put *after* the closing punctuation in block quotes, but *before* closing punctuation when inline.

* Latin-isms should be avoided when possible. Change "cf." to "compare"; "ibid." to simply a repeated reference.

* If the identity of the author is very salient from context, or there are a number of repeated references, one can cite just the year `\citeyearpar[5]{soandso2004}`. There are no hard and fast rules for when to use this shortening. However, the name or names should *always* be given after a block quote.

* A single "f." should never be used in a page citation: use 130–31, not 130f. Avoid double "ff."s as well if you can, but it may not be worth bugging the author for an exact range.


## More about the JHAP document class, `jhap.cls`

* The document class defines the commands `\author{...}`, `\email{...}`, `\affiliation{...}`, `\title{...}`. `\volume{...}`, `\volnumber{...}`, and when there is more than one author, `\secondauthor{...}`, `\secondaffiliation{affiliation}`, `\secondemail{...}`; for special issues there are `\specialvolume{...}`, `\specialvolumeeditors{...}`, and for reviews, `\reviewedtitle{...}`, `\reviewedauthor{...}` (or `\reviewededitor{...}`; use one or the other not both), `\reviewedpubdetails{...}`, as well as `\secondreviewedauthor{...}`, etc. Generally, these are automatically inserted when the LaTeX file is first created, using the information provided on the metadata page, but may be added or changed later if needed.

* The class is based on the `article` class, and therefore defines all the commands available in it.

* The class additionally loads the following LaTeX packages, which need not (and should not) be separately loaded: `geometry`, `inputenc`, `fontenc`, `graphicx`, `amssymb` (and thus `amsfonts`), `fancyhdr`, `titlesec`, `natbib`, `enumitem`, `etoolbox`, `tocloft`, and `hyperref`. Additional packages may be loaded in the preamble as needed. The package `philex` is often useful if an author uses numbered examples and refers back to them.

* Unlike some classes, the `\begin{abstract}...\end{abstract}` environment goes before `\maketitle`.

* The copyright notice at the bottom can be changed by redefined the command `\copynotice`, i.e., `\renewcommand{\copynotice}{Custom notice}`.

* Unlike the usual `\begin{quote}` ... `\end{quote}` environment, this one defined in the class allows paragraph breaks. (So there is no need ever to use the `quotation` environment.) However, commands such as `\medskip` should be used to place space between the paragraphs if desired. (You might also use `\smallskip` or `\bigskip` depending on the space constraints on the given page and desired aesthetic look.)

* The class defines a command `\nudge` that can be used to insert a very small amount of space, even less than a thin space, which is useful for italic corrections and visible distinguishing successtive beginning or ending quotatation marks (e.g., `'\nudge''` for an ending single quotation mark followed immediately by an ending double quotation mark).

## Source locations

The latest version of `jhap.cls` and this guide can be found in this GIT repository:

[https://bitbucket.org/frabjous/jhap-cls/](https://bitbucket.org/frabjous/jhap-cls/)

Source code for the JHAP typesetting framework can be found in this GIT repository:

[https://bitbucket.org/frabjous/journal-tools/](https://bitbucket.org/frabjous/journal-tools/)

Links to necessary libraries and other software can be found in the `README.md` file there.

All custom software is licensed [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html).