This is the LaTeX document class used to typeset artices for the [Journal for the History of Analytical Philosophy](https://jhaponline.org).

A guide for using it (`jhap_typesetting_guide.md`) is included in the repository.

License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)